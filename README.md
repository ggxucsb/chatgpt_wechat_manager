
<h4 align="center">后端管理页面</h4>
所有代码编译包均在:
**链接：https://pan.baidu.com/s/1yjhYu9aaAaI1GWPne3PJYg 提取码：1111**
## 后端介绍

* 微信小程序前端: [点我进入](https://gitee.com/e0cia/chatgpt_wechat_font)

* 微信小程序后端源码: [点我进入](https://gitee.com/e0cia/chatgpt_wechat_houtai)
* github地址： [点我进入](https://github.com/e0cia)

小程序需配合此套java程序，下面将详细介绍此套系统的搭建过程谢谢！
如果前端的话请大家转移的上述的前端链接，获取小程序源码




## 搭建教学
###  **_安装教程_**  : [点我进入](https://wiki.circlai.com/project-1/)

## 小程序简介

**功能简介** 

*1.角色选择

*2.优化UI和角色预设

*3.增加stream流式传输

*4.集成模型包括gpt3.5（初版大更新gpt4、绘画模型、文心一言等暂时没有加、下个版本就会加入）

*5.历史对话记录

*6.我的问询记录

*7.支持三方接口

*8.增加会员中心

*9.展示剩余次数

*10.增加次数和时间两种计费模式,后台可行性选择

*11.增加充值（目前仅支持充值卡重置）功能

*12.分享增加次数功能

*13.是否计费对话、是否计费询问、公告显示等均可自定义开关

*14.后台自行添加模型分类、模型名称、设定语等等

*15.所有后台配置页面化

*16.用户管理

*17.在线用户

*18.登录日志

*19.后台批量生成激活码

*20.激活码一键导出

*21.后台key统一管理

*22.key自动轮询

*23.使用的为微信官方违禁词，可自行添加违禁词


## 交流群

QQ群： ![加入QQ群](https://yuan-ai.oss-cn-beijing.aliyuncs.com/qqgroup.jpg)
微信群： ![加入QQ群](https://yuan-ai.oss-cn-beijing.aliyuncs.com/qqgroup.jpg)

## 小程序演示
<table>
    <tr>
        <td><img src="https://image.hongchiqingyun.com/gh_35c30216652f_258.jpg"/></td>
    </tr>
</table>

<table>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/1.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/2.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/4.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/5.png"/></td>        
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/6.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/7.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/8.png"/></td> 
         <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/9.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/10.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/qiantai/11.png"/></td> 
    </tr>	 
 
</table>




## 后台演示
<table>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/1.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/2.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/4.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/5.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/6.png"/></td>
    </tr>
     <tr>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/7.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/8.png"/></td>
        <td><img src="https://yuan-ai.oss-cn-beijing.aliyuncs.com/houtai/9.png"/></td>
    </tr>
</table>
